import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

import NotFound from "./components/NotFound.vue";

// Books
import EditBook from "./components/books/EditBook.vue";
import CreateBook from "./components/books/CreateBook.vue";
import DisplayBook from "./components/books/DisplayBook.vue";
import DisplayBooks from "./components/books/DisplayBooks.vue";

Vue.use(Router);

export default new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/",
			name: "home",
			component: Home,
		},
		{
			path: "/books",
			name: "display-books",
			component: DisplayBooks,
		},
		{
			path: "/books/create",
			name: "create-books",
			component: CreateBook,
		},
		{
			path: "/books/:slug",
			name: "display-book",
			component: DisplayBook,
		},
		{
			path: "/books/:slug/edit",
			name: "edit-book",
			component: EditBook,
		},
		{
			path: "*",
			redirect: "/404",
		},
		{
			path: "/404",
			name: "404",
			component: NotFound,
		},
	],
});
